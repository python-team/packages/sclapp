# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os, sys, signal, errno
from unittest import main

from common import (
  logSignals,
  execHead,
  waitForPid,
  assertLogFileContainsExactly,
  assertSignalCaught,
  assertLogFileContains,
  SclappTestCase,
)
from manager import manager

import sclapp

def _main(argv):
    import time
    sclapp.debug()
    try:
        for i in range(30):
            try:
                print 'foo'
                sys.stdout.flush()
            except sclapp.CriticalError:
                pass
            except IOError, e:
                if e.errno != errno.EPIPE:
                    raise
                break

            try:
                print >>sys.stderr, 'bar'
                sys.stderr.flush()
            except sclapp.CriticalError:
                pass
            except IOError, e:
                if e.errno != errno.EPIPE:
                    raise
                break

            time.sleep(0.2)
    finally:
        time.sleep(2)
        logSignals()
    return 0

# Use announce_signals = False to prevent signal announcement (on stderr) from
# disturbing our stdio testing.

main_with_signal_handling_with_output_protection = sclapp.mainWrapper(
  _main,
  exit_signals = [signal.SIGPIPE],
  protect_output = True,
  announce_signals = False,
  bug_message = None,
)
main_with_signal_handling_without_output_protection = sclapp.mainWrapper(
  _main,
  exit_signals = [signal.SIGPIPE],
  protect_output = False,
  announce_signals = False,
  bug_message = None,
)
main_without_signal_handling_with_output_protection = sclapp.mainWrapper(
  _main,
  handle_signals = False,
  protect_output = True,
  bug_message = None,
)
main_without_signal_handling_without_output_protection = sclapp.mainWrapper(
  _main,
  handle_signals = False,
  protect_output = False,
  bug_message = None,
)

def _stdio_fails(main_fn, pipe_out = True, pipe_err = False):
    from sclapp.debug_logging import DEBUG_LOGFILE
    from sclapp import pipes as s_p

    fns = [ main_fn, execHead ]
    argses = [ None, [ 2 ] ]
    kwargses = [ None, None ]
    pid = s_p.pipeFns(
      fns, argses, kwargses,
      pipe_out = pipe_out, pipe_err = pipe_err,
      stdin = '/dev/null', stdout = DEBUG_LOGFILE, stderr = DEBUG_LOGFILE
    )
    waitForPid(pid)
    return pid

def _stdout_fails(main_fn):
    return _stdio_fails(main_fn)

def _stdout_stderr_fail(main_fn):
    return _stdio_fails(main_fn, pipe_err = True)

def _make_test_fn(name, failure_fn, main_fn, foos, bars, signals,
  exceptions = ()):
    def test_fn():
        pid = failure_fn(main_fn)
        assertLogFileContainsExactly('foo', foos)
        assertLogFileContainsExactly('bar', bars)
        for signal in signals:
            assertSignalCaught(signal, pid)
        for exception in exceptions:
            assertLogFileContains(exception.__name__)
    test_fn.__name__ = name
    return test_fn

class TestOutputProtection(SclappTestCase):
    test_stdout_fails_with_signal_handling_with_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_fails_with_signal_handling_with_output_protection',
        _stdout_fails,
        main_with_signal_handling_with_output_protection,
        foos = 2, bars = 30, signals = (signal.SIGPIPE,)
    )))
    test_stdout_stderr_fail_with_signal_handling_with_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_stderr_fail_with_signal_handling_with_output_protection',
        _stdout_stderr_fail,
        main_with_signal_handling_with_output_protection,
        foos = 1, bars = 1, signals = (signal.SIGPIPE,)
    )))
    test_stdout_fails_with_signal_handling_without_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_fails_with_signal_handling_without_output_protection',
        _stdout_fails,
        main_with_signal_handling_without_output_protection,
        foos = 0, bars = 0, signals = (), exceptions = (AssertionError,)
    )))
    # Don't expect to hear about the AssertionError since stderr is borked.
    test_stdout_stderr_fail_with_signal_handling_without_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_stderr_fail_with_signal_handling_without_output_protection',
        _stdout_stderr_fail,
        main_with_signal_handling_without_output_protection,
        foos = 0, bars = 0, signals = ()
    )))

    # Note that, depending upon buffering of stdio, we may or may not get an
    # IOError when writing into a broken pipe with signals disabled.

    test_stdout_fails_without_signal_handling_with_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_fails_without_signal_handling_with_output_protection',
        _stdout_fails,
        main_without_signal_handling_with_output_protection,
        2, 30, ()
    )))
    test_stdout_stderr_fail_without_signal_handling_with_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_stderr_fail_without_signal_handling_with_output_protection',
        _stdout_stderr_fail,
        main_without_signal_handling_with_output_protection,
        1, 1, ()
    )))
    test_stdout_fails_without_signal_handling_without_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_fails_without_signal_handling_without_output_protection',
        _stdout_fails,
        main_without_signal_handling_without_output_protection,
        2, 2, ()
    )))
    test_stdout_stderr_fail_without_signal_handling_without_output_protection = (
      staticmethod(_make_test_fn(
        'test_stdout_stderr_fail_without_signal_handling_without_output_protection',
        _stdout_stderr_fail,
        main_without_signal_handling_without_output_protection,
        1, 1, ()
    )))

manager.add_test_case_class(TestOutputProtection)
