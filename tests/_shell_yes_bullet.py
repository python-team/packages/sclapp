from sclapp.shell import Shell
from sclapp import stdio_encoding

def main():
    sh = Shell()
    stdio_encoding.enableStdioEncoding()
    sh.followWrite(u'yes \u2022 2>/dev/null | head -n10')

if __name__ == '__main__':
    main()
