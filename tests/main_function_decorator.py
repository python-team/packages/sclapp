# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os, sys
from unittest import main

from common import (
  SclappTestCase,
  waitForPid,
  assertLogFileContains,
  assertLogFileDoesNotContain,
  removeLogFile,
)
from manager import manager

import sclapp
from sclapp import debug_logging

@sclapp.main_function
def _main(argv):
    print 'testing'
    return 0

class MainFunctionDecoratorTestCase(SclappTestCase):
    @staticmethod
    def test_noargs():
        from sclapp import pipes as s_p
        pid = os.fork()
        if not pid:
            s_p.redirectFds(
              stdin = '/dev/null',
              stdout = debug_logging.DEBUG_LOGFILE,
              stderr = debug_logging.DEBUG_LOGFILE
            )
            _main()
            os._exit(0)
        waitForPid(pid)
        assertLogFileContains('testing')
        assertLogFileDoesNotContain('Traceback')
        removeLogFile()

manager.add_test_case_class(MainFunctionDecoratorTestCase)
