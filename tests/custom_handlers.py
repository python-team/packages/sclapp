# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os, sys, signal, time
from unittest import main

from manager import manager

__doc__ = '''
>>> import sclapp

>>> def myhandler(signum, frame):
...     print 'myhandler'

>>> sclapp.enableSignalHandling(
...   notify_signals = (signal.SIGINT,),
...   custom_handlers = {signal.SIGHUP: myhandler},
...   announce_signals = False
... )

>>> def f():
...     os.kill(os.getpid(), signal.SIGINT)
...     time.sleep(2)
>>> f()
Traceback (most recent call last):
...
SignalError: caught signal 2

>>> def f():
...     os.kill(os.getpid(), signal.SIGHUP)
...     time.sleep(2)
>>> f()
myhandler

>>> sclapp.disableSignalHandling()
'''

manager.add_doc_test_cases_from_string(
  __doc__,
  globs = globals(),
  name = __name__,
)
