import os
from unittest import main

from common import PROJECT_DIR
from manager import manager

doc_filename = os.path.join(PROJECT_DIR, 'sclapp.txt')

manager.add_doc_test_cases_from_text_file(doc_filename, name = __name__)
