# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

__author__ = 'Forest Bond'

import os, sys, signal
from unittest import main

import sclapp

from common import (
  SclappTestCase,
  redirectToLogFile,
  waitForPid,
  assertLogFileContains,
)
from manager import manager

@sclapp.main_function(author = __author__, exit_signals = [ signal.SIGINT ])
def _main(argv):
    import time
    while True:
        print 'testing bug handling'
        time.sleep(1)
        this_should_fail()

class BugHandlingTestCase(SclappTestCase):
    @staticmethod
    def test_basic_handling():
        sclapp.setErrorOutputLevel(sclapp.WARNING)
        pid = os.fork()
        if not pid:
            redirectToLogFile()
            _main()
            os._exit(0)
        waitForPid(pid)
        assertLogFileContains('Traceback')
        assertLogFileContains('file a bug report')
        assertLogFileContains(__author__)

manager.add_test_case_class(BugHandlingTestCase)
