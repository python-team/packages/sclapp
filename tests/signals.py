# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os, sys, signal
from unittest import main

import sclapp

from common import (
  logSignals,
  SclappTestCase,
  waitForPid,
  assertSignalCaught,
  execHead,
)
from manager import manager

@sclapp.main_function(notify_signals = [signal.SIGHUP, signal.SIGPIPE])
def _main(argv):
    import time
    sclapp.debug()
    try:
        for i in range(20):
            try:
                print 'foo'
                print >>sys.stderr, 'bar'
                os.kill(os.getpid(), signal.SIGHUP)
            except sclapp.SignalError, e:
                if e.signum == signal.SIGHUP:
                    pass
                if e.signum == signal.SIGPIPE:
                    pass
            time.sleep(0.5)
    finally:
        logSignals()
    return 0

class SignalsTestCase(SclappTestCase):
    @staticmethod
    def test_notify_sighup():
        from sclapp.debug_logging import DEBUG_LOGFILE
        from sclapp import pipes as s_p

        pid = os.fork()
        if not pid:
            s_p.redirectFds(
              stdin = '/dev/null', stdout = DEBUG_LOGFILE, stderr = DEBUG_LOGFILE)
            _main()
            os._exit(0)
        waitForPid(pid)
        assertSignalCaught(signal.SIGHUP, pid)

    @staticmethod
    def test_notify_sighup_sigpipe():
        from sclapp.debug_logging import DEBUG_LOGFILE
        from sclapp import pipes as s_p

        fns = [ _main, execHead ]
        argses = [ None, [ 2 ] ]
        kwargses = [ None, None ]
        pid = s_p.pipeFns(fns, argses, kwargses,
          stdin = '/dev/null', stdout = DEBUG_LOGFILE, stderr = DEBUG_LOGFILE)
        waitForPid(pid)
        assertSignalCaught(signal.SIGHUP, pid)
        assertSignalCaught(signal.SIGPIPE, pid)

    @staticmethod
    def test_string_signal_mapping_specification():
        import time

        sclapp.enableSignalHandling(
          notify_signals = ['SIGALRM'],
          announce_signals = False,
        )

        try:
            signal.alarm(1)
            time.sleep(3)
        except sclapp.SignalError:
            pass
        else:
            self.fail('Expected to receive SIGALRM.')

        sclapp.disableSignalHandling()

manager.add_test_case_class(SignalsTestCase)
