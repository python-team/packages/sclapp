# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import sys, os, signal, errno

from sclapp.exceptions import CriticalError
import sclapp.signals

from sclapp.util import DelegateWrapper

_protected_output_enabled = False

class _FileWrapper(DelegateWrapper):
    buffer = [ ]

    def __init__(self, wrapped_obj):
        self.wrapped_obj = wrapped_obj

    def write(self, message):
        pass

    def flush(self):
        pass

    def signalOutput(self):
        pass

    def restoreOutput(self):
        pass

    def disableOutput(self):
        self.__class__ = _NullFileWrapper

class _NullFileWrapper(_FileWrapper):
    def disableOutput(self):
        pass

class _SafeFileWrapper(_FileWrapper):
    def write(self, message):
        try:
            self.wrapped_obj.write(message)
            self.wrapped_obj.flush()
        except IOError, e:
            if e.errno != errno.EPIPE:
                raise
            if hasattr(signal, 'SIGPIPE') and (
              signal.SIGPIPE in sclapp.signals.getExitSignals()):
                # We want to disable output for stdout, stderr if they are
                # triggering IOError EPIPE exceptions, but we need to wait
                # until a SIGPIPE signal is caught on POSIX systems.
                if signal.SIGPIPE in sclapp.signals.getCaughtSignals():
                    self.disableOutput()

                # The IOError we just caught may have interrupted handling of
                # an ExitSignalError.  If so, we should re-raise it to ensure
                # proper handling of that exception.
                if sclapp.signals._exit_signal_exception is not None:
                    raise sclapp.signals._exit_signal_exception
            else:
                self.disableOutput()
                raise CriticalError, (e.errno, unicode(e))

    def signalOutput(self):
        self.__class__ = _SignalFileWrapper

class _SignalFileWrapper(_FileWrapper):
    def write(self, message):
        self.buffer.append(message)
        self.flush()

    def restoreOutput(self):
        self.__class__ = _SafeFileWrapper
        while len(self.buffer) > 0:
            self.write(self.buffer.pop())

    def flush(self):
        if sclapp.signals._handling_signal is None:
            self.restoreOutput()

def _protectOutput():
    global _protected_output_enabled
    sys.stdout = _SafeFileWrapper(sys.stdout)
    sys.stderr = _SafeFileWrapper(sys.stderr)
    _protected_output_enabled = True

def protectOutput():
    if not _protected_output_enabled:
        return _protectOutput()

def _unprotectOutput():
    global _protected_output_enabled
    sys.stdout = sys.stdout.wrapped_obj
    sys.stderr = sys.stderr.wrapped_obj
    _protected_output_enabled = False

def unprotectOutput():
    if _protected_output_enabled:
        _unprotectOutput()

def _signalOutput():
    sys.stdout.signalOutput()
    sys.stderr.signalOutput()
