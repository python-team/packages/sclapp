# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import logging

def setErrorOutputLevel(level):
    '''setErrorOutputLevel(level) -> None

    level: int minimum priority of error messages that are displayed

    Sets the minimum priority required for an error output message to actually
    be seen.  sclapp defines the following constants for convenience:

    ALL = 1
    SCLAPP_DEBUG = 5
    DEBUG = 10
    INFO = 20
    WARNING = 30
    ERROR = 40
    CRITICAL = 50
    '''
    return _logger.setLevel(level)

ALL = 1
SCLAPP_DEBUG = 5
DEBUG = logging.DEBUG
INFO = logging.INFO
WARNING = logging.WARNING
ERROR = logging.ERROR
CRITICAL = logging.CRITICAL

_logger = logging.getLogger('sclapp')
_console = logging.StreamHandler()
_formatter = logging.Formatter('%(message)s')
_console.setFormatter(_formatter)
_logger.addHandler(_console)
setErrorOutputLevel(DEBUG)

def _printSclappDebug(message):
    '''_printSclappDebug(message) -> None

    Prints a message to stderr with priority SCLAPP_DEBUG.
    '''
    return _logger.log(SCLAPP_DEBUG, message)

def printDebug(message):
    '''printDebug(message) -> None

    Prints a message to stderr with priority DEBUG.
    '''
    return _logger.debug(message)

def printInfo(message):
    '''printInfo(message) -> None

    Prints a message to stderr with priority INFO.
    '''
    return _logger.info(message)

def printWarning(message):
    '''printWarning(message) -> None

    Prints a message to stderr with priority WARNING.
    '''
    return _logger.warning(message)

def printError(message):
    '''printError(message) -> None

    Prints a message to stderr with priority ERROR.
    '''
    return _logger.error(message)

def printCritical(message):
    '''printCritical(message) -> None

    Prints a message to stderr with priority CRITICAL.
    '''
    return _logger.critical(message)
