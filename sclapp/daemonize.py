# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os, sys

from sclapp.error_output import printInfo
from sclapp.processes import redirectFds

def getRootDirectory():
    '''Tries to find the root directory by moving up the directory tree starting
    from the current working directory.
    '''
    p = os.getcwd()
    _p = None
    while p != _p:
        _p = p
        p = os.path.dirname(p)
    return p

def daemonize():
    '''Performs traditional *nix daemonization.  For more information, see
    http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16.
    '''
    printInfo('daemonizing...')
    if os.fork():
        os._exit(0)
    os.setsid()
    if os.fork():
        os._exit(0)
    os.chdir(getRootDirectory())         
    os.umask(0)
    redirectFds(os.devnull, os.devnull, os.devnull)
