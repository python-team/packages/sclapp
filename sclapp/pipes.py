# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

'''A module that implements some strange functions for turning Python functions
into sub-processes that can be strung together with pipes.  Not recommended for
mass-consumption at this time.
'''

import os, sys

from processes import waitPid
from redirection import redirectFds

def pipeFnsRecursive(fns, argses = None, kwargses = None, pipe_out = True,
  pipe_err = False):
    if argses is None:
        argses = len(fns) * ( ( ), )
    if kwargses is None:
        kwargses = len(fns) * ( { }, )
    if len(fns) > 1:
        next_pipe_w = pipeFnsRecursive(fns[1:], argses[1:], kwargses[1:])
    pipe_r, pipe_w = os.pipe()
    pid = os.fork()
    if not pid:
        #child
        os.close(pipe_w)
        if len(fns) > 1:
            stdout = None
            stderr = None
            if pipe_out:
                stdout = next_pipe_w
            if pipe_err:
                stderr = next_pipe_w
            execFnFds(fns[0], argses[0], kwargses[0], stdin = pipe_r,
              stdout = stdout, stderr = stderr)
        else:
            execFnFds(fns[0], argses[0], kwargses[0], stdin = pipe_r)
    else:
        #parent
        os.close(pipe_r)
        return pipe_w

def pipeFns(fns, argses = None, kwargses = None, pipe_out = True,
  pipe_err = False, stdin = None, stdout = None, stderr = None):
    pid = os.fork()
    if not pid:
        redirectFds(stdin = stdin, stdout = stdout, stderr = stderr)
        if argses is None:
            argses = len(fns) * ( ( ), )
        if kwargses is None:
            kwargses = len(fns) * ( { }, )
        if len(fns) > 1:
            pipe_w = pipeFnsRecursive(
              fns[1:], argses[1:], kwargses[1:], pipe_out = pipe_out,
              pipe_err = pipe_err)
        else:
            pipe_w = None
        stdout = None
        stderr = None
        if pipe_out:
            stdout = pipe_w
        if pipe_err:
            stderr = pipe_w
        execFnFds(
          fns[0], argses[0], kwargses[0], stdout = stdout, stderr = stderr)
    return pid

def pipeCmds(cmds, argses, pipe_out = True, pipe_err = False, stdin = None,
  stdout = None, stderr = None):
    def execWrapper(*args, **kwargs):
        print 'execing %s' % args[0]
        print >>sys.stderr, 'execing %s' % args[0]
        os.execvp(*args, **kwargs)
    fns = len(cmds) * ( execWrapper, )
    fn_argses = zip(cmds, argses)
    return pipeFns(fns, fn_argses, pipe_out = pipe_out, pipe_err = pipe_err,
      stdin = stdin, stdout = stdout, stderr = stderr)

def execFnFds(fn, args = None, kwargs = None, stdin = None, stdout = None,
  stderr = None):
    redirectFds(stdin = stdin, stdout = stdout, stderr = stderr)
    if args is None:
        args = [ ]
    if kwargs is None:
        kwargs = { }
    status = fn(*args, **kwargs)
    try:
        os._exit(status)
    except TypeError:
        os._exit(-1)

if __name__ == '__main__':
    def test01():
        print pipeCmds(['echo'], [['echo', 'test01']])
        print pipeCmds(['echo', 'grep'], [['echo', 'test01'], ['grep', 'hi']])
        print pipeCmds(
          ['echo', 'grep'], [['echo', 'test01'], ['grep', 'test01']])
    def test02():
        def f(x):
            print x
        pid = os.fork()
        if not pid:
            pipeFns([f, os.execvp], [['test02'], ['grep', ['grep', 'test02']]])
        print pid
    test01()
    test02()
    test01()
