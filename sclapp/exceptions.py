# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

'''This module implements sclapp exceptions.'''

from sclapp.util import safe_encode, safe_decode

class Error(Exception):
    '''Base exception for other exceptions defined in this module.'''

class CriticalError(Error):
    '''Indicates a fatal error.'''

    code = None
    message = None

    def __init__(self, code = 0, message = u''):
        if type(code) != int:
            raise ValueError, (
              u'first argument to CriticalError initializer '
              'should be an integer'
            )
        self.code = code
        self.message = message
        Error.__init__(self)

    def __str__(self):
        return safe_encode(self.message)

    def __unicode__(self):
        return safe_decode(self.message)

class UsageError(CriticalError):
    '''Indicates a usage error.'''

    def __init__(self, message = None):
        if message is not None:
            code = -1
        else:
            code = 0
        CriticalError.__init__(self, code, message)

class SignalError(Error):
    '''Indicates that a notify signal has been caught.'''

    def __init__(self, signum):
        self.signum = signum
        Error.__init__(self)

    def __str__(self):
        return 'caught signal %i' % self.signum

class ExitSignalError(CriticalError):
    '''Indicates that an exit signal has been caught.'''

    def __init__(self, signum):
        self.signum = signum
        CriticalError.__init__(self)

    def __str__(self):
        return 'caught exit signal %i' % self.signum
