# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import sys, os, types

from sclapp import error_output

def _dup(n, f, flags = None):
    os.close(n)
    try:
        return os.dup(f.fileno())
    except (AttributeError, TypeError):
        try:
            return os.dup(f)
        except TypeError:
            try:
                return os.open(f, flags)
            except TypeError:
                raise TypeError, (
                  'f is neither a usable file-like object, '
                  'an integer file descriptor, nor a filename.'
                )

def redirectFds(stdin = None, stdout = None, stderr = None):
    '''Redirects the standard I/O file descriptors for the current process.
    stdin, stdout, and stderr can be either a filename, an fd (integer), or
    None (to indicate no redirection).
    '''
    pid = os.getpid()
    if stdin is not None:
        error_output._printSclappDebug(
          u'pid %u: redirecting stdin to %s' % (pid, stdin))
        _dup(0, stdin, os.O_RDONLY)
    if stdout is not None:
        error_output._printSclappDebug(
          u'pid %u: redirecting stdout to %s' % (pid, stdout))
        _dup(1, stdout, os.O_RDWR | os.O_APPEND | os.O_CREAT)
    if stderr is not None:
        error_output._printSclappDebug(
          u'pid %u: redirecting stderr to %s' % (pid, stderr))
        _dup(2, stderr, os.O_RDWR | os.O_APPEND | os.O_CREAT)
