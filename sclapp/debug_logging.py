# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os

DEBUG_LOGFILE = u'logfile'

def logMessage(message):
    logfile = file(DEBUG_LOGFILE, 'a')
    logfile.write(u'%s\n' % message)
    logfile.close()

def readLogFile():
    logfile = file(DEBUG_LOGFILE, 'r')
    contents = logfile.read()
    logfile.close()
    return contents

def removeLogFile():
    return os.remove(DEBUG_LOGFILE)

def logWrapper(fn):
    def newFn(*args, **kwargs):
        logMessage(u'entering %s' % fn.__name__)
        logMessage(u'args: %s' % unicode(args))
        logMessage(u'kwargs: %s' % unicode(kwargs))
        try:
            return fn(*args, **kwargs)
        finally:
            logMessage(u'exiting %s' % fn.__name__)
    return newFn
