# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import locale
from UserDict import DictMixin

def safe_encode(s):
    s = safe_decode(s)
    return s.encode(locale.getpreferredencoding(), 'replace')

def safe_decode(s):
    if type(s) is unicode:
        return s
    return unicode(str(s), locale.getpreferredencoding(), 'replace')

def importName(name):
    '''Imports a Python module referred to by name, and returns that module.
    Raises an ImportError if the import fails.
    '''
    mod = __import__(name)
    components = name.split('.')
    for comp in components[1:]:
        try:
            mod = getattr(mod, comp)
        except AttributeError:
            raise ImportError, '%s has no attribute %s' % (mod, comp)
    return mod

class DelegateWrapper(object):
    wrapped_obj = None

    def __getattr__(self, name):
        # Note that this only gets called if Python failed to find an attribute
        # using the normal mechanisms.  Thus, only those attributes not defined
        # by the original class definition are gotten this way.
        return getattr(self.wrapped_obj, name)

    def __setattr__(self, name, value):
        # This prevents setting attributes that aren't present in the class
        # definition:
        for cls in [ self.__class__ ] + list(self.__class__.__bases__):
            if hasattr(cls, name):
                object.__setattr__(self, name, value)
                return
        setattr(self.wrapped_obj, name, value)

class CallbackMapping(object, DictMixin):
    def __init__(self, keys, getitem, setitem = None, delitem = None):
        if setitem is None:
            def setitem(name, value):
                raise NotImplementedError

        if delitem is None:
            def delitem(name):
                raise NotImplementedError

        self._keys = keys
        self._getitem__ = getitem
        self._setitem__ = setitem
        self._delitem__ = delitem

    def keys(self):
        return self._keys()

    def __getitem__(self, name):
        return self._getitem__(name)

    def __setitem__(self, name, value):
        self._setitem__(name, value)

    def __delitem__(self, name):
        self._delitem__(name)
