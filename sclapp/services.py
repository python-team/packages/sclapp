# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import sys, os, signal
from sclapp.daemonize import daemonize
from sclapp.exceptions import *
from sclapp.error_output import *

class InvalidPidFileError(Error):
    pass

def readPidFile(pid_file_name):
    try:
        f = open(pid_file_name, 'r')
    except IOError:
        return None
    try:
        contents = f.read().strip()
        assert(len(contents.split(u'\n')) < 2)
        f.close()
        pid = int(contents)
    except (AssertionError, ValueError):
        raise InvalidPidFileError
    return pid

def writePidFile(pid_file_name, pid = None):
    if pid is None:
        pid = os.getpid()
    pid_file = open(pid_file_name, 'w')
    pid_file.write(unicode(os.getpid()))
    pid_file.close()

def removePidFile(pid_file_name):
    try:
        os.unlink(pid_file_name)
    except OSError, e:
        if e.errno != 2:
            raise

def startService(pid_file_name, fn, args = None, kwargs = None):
    if args is None:
        args = [ ]
    if kwargs is None:
        kwargs = { }

    # TODO: verify that this pid actually belongs to an instance of the service.
    pid = readPidFile(pid_file_name)
    if pid is not None:
        raise CriticalError, (1, u'already running? process ID %u' % pid)

    pid = os.fork()

    if not pid:
        # child process
        daemonize()

        try:
            writePidFile(pid_file_name, pid = pid)
            fn(*args, **kwargs)
        finally:
            try:
                removePidFile(pid_file_name)
            except Exception, e:
                printCritical(unicode(e))
            os._exit(0)

def stopService(pid_file_name):
    try:
        pid_file = open(pid_file_name, 'r')
    except IOError:
        return
    pid = int(pid_file.read().strip())
    pid_file.close()
    # FIXME: wait for process to die, try to kill again if necessary
    try:
        os.kill(pid, signal.SIGINT)
    except OSError:
        pass
