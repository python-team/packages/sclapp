# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

'''An easy-to-use framework for python command line applications.'''

# XXX: This work-around causes darwin to use the LC_* and LANG environment
# variables as on other Unix-like systems.  Without this,
# locale.getpreferredencoding always returns "mac-roman".  See:
#
#  http://www.selenic.com/mercurial/wiki/index.cgi/Character_Encoding_On_OSX

import sys

if sys.platform == 'darwin':
    sys.platform = 'generic'
    import locale
    sys.platform = 'darwin'
else:
    import locale

__all__ = (
  '__version__',
  # from sclapp.exceptions:
  'CriticalError',
  'UsageError',
  'SignalError',
  'ExitSignalError',
  # from sclapp.protected_output:
  'protectOutput',
  'unprotectOutput',
  # from sclapp.error_output:
  'printDebug',
  'printInfo',
  'printWarning',
  'printError',
  'printCritical',
  'setErrorOutputLevel',
  'ALL',
  'SCLAPP_DEBUG',
  'DEBUG',
  'INFO',
  'WARNING',
  'ERROR',
  'CRITICAL',
  # from sclapp.signals:
  'getExitSignals',
  'getNotifySignals',
  'getDefaultSignals',
  'getIgnoreSignals',
  'getCaughtSignals',
  'enableSignalHandling',
  'disableSignalHandling',
  'ALL_SIGNALS',
  'STD_EXIT_SIGNALS',
  'STD_NOTIFY_SIGNALS',
  'STD_DEFAULT_SIGNALS',
  'STD_IGNORE_SIGNALS',
  # from sclapp.main:
  'mainWrapper',
  'main_function',
  'makeSubCommandMain',
  # public sub-modules:
  'daemonize',
  'processes',
  'redirection',
  'termcontrol',
  'services',
  'stdio_encoding',
  'shinterp',
  # Allow callers to import locale from sclapp (see work-around above):
  'locale',
)

from sclapp.main import mainWrapper, main_function, makeSubCommandMain
from sclapp import termcontrol, services, daemonize, processes, redirection, \
  stdio_encoding, shinterp
  
try:
    import shell
except ImportError:
    pass
else:
    __all__ = __all__ + ('shell',)

from sclapp.exceptions import CriticalError, UsageError, SignalError, \
  ExitSignalError
from sclapp.protected_output import protectOutput, unprotectOutput
from sclapp.error_output import printDebug, printInfo, printWarning, \
  printError, printCritical, setErrorOutputLevel, \
  ALL, SCLAPP_DEBUG, DEBUG, INFO, WARNING, ERROR, CRITICAL
from sclapp.signals import getExitSignals, getNotifySignals, \
  getDefaultSignals, getIgnoreSignals, getCaughtSignals, \
  enableSignalHandling, disableSignalHandling, \
  ALL_SIGNALS, STD_EXIT_SIGNALS, STD_NOTIFY_SIGNALS, STD_DEFAULT_SIGNALS, \
  STD_IGNORE_SIGNALS
from sclapp import signals

try:
    from sclapp import version
except ImportError:
    __version__ = '0.0.0'
else:
    __version__ = version.version

def debug():
    '''Causes sclapp to enter debug mode.  Currently, this means that sclapp
    will wrap it's signal handlers with the logWrapper() function from the
    sclapp.debug_logging module.
    '''
    from sclapp.debug_logging import logWrapper
    signals._exitSignalHandler = logWrapper(
      signals._exitSignalHandler)
    signals._notifySignalHandler = logWrapper(
      signals._notifySignalHandler)
    signals._ignoreExitSignalHandler = logWrapper(
      signals._ignoreExitSignalHandler)
