# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import os, sys, re, random
import string, locale, struct, fcntl, termios, signal

from pexpect import (
  spawn,
  TIMEOUT,
  EOF,
)

from sclapp import shinterp
from sclapp.exceptions import Error
from sclapp.util import DelegateWrapper, CallbackMapping
from sclapp.error_output import printError

RAND_PROMPT_SIZE = 32

MAX_DELAY = 1200

def randomAlphaNumeric(length):
    return ''.join(random.sample(string.letters + string.digits, length))

class CommandFailed(Error):
    pass

class CommandSignalled(Error):
    pass

class CommandFinished(Error):
    pass

class ExitInteract(Error):
    pass

def lengthOfLongestLine(s):
    length = 0
    for line in s.split('\n'):
        len_line = len(line)
        if len_line > length:
            length = len_line
    return length

class Shell(spawn):
    shell = None
    _prompt = None

    failure_exceptions = None
    signal_exceptions = None
    trace = None

    interpolate = staticmethod(shinterp.interpolate)

    env = None

    timeout_cache = None
    dir_cache = None

    shell_encoding = 'utf-8'

    always_group_cmd = None

    interactive_ps1 = '$ '
    interactive_ps2 = '> '
    interactive_ps3 = ''
    interactive_ps4 = '+ '

    interactive_exit_string = 'DEBSYS_INTERACTIVE_SHELL_EXITING'

    def __init__(self, shell = '/bin/sh', prompt = None,
      failure_exceptions = True, signal_exceptions = True, trace = False,
      timeout = 30, shell_encoding = None, always_group_cmd = True,
      delaybeforesend = None, **kwargs
    ):
        if prompt is None:
            prompt = randomAlphaNumeric(RAND_PROMPT_SIZE)

        self.failure_exceptions = failure_exceptions
        self.signal_exceptions = signal_exceptions
        self.trace = trace

        self.shell = shell
        super(Shell, self).__init__(self.shell, **kwargs)

        if delaybeforesend is not None:
            self.delaybeforesend = delaybeforesend

        self.timeout_cache = [ ]
        self._pushTimeout(timeout)

        self._pre_prompt()

        self.setecho(False)
        self.prompt = prompt

        self.env = CallbackMapping(
          self._envKeys, self._envGet, self._envSet, self._envDel)

        self.dir_cache = [ ]

        if shell_encoding is None:
            self.shell_encoding = self.detectShellEncoding()
        else:
            self.shell_encoding = shell_encoding

        self.env['PS2'] = ''
        self.env['PS3'] = ''
        self.env['PS4'] = ''

        if 'TERM' in os.environ:
            self.env['TERM'] = os.environ['TERM']

        self.always_group_cmd = always_group_cmd

        self.setupShell()

    def __del__(self):
        # Make an effort to clean up here just in case our caller neglected to
        # do so.  If we don't, bad things (like not closing the pty) can
        # happen.
        try:
            self.exit()
        except Exception, e:
            printError(e)

    def setupShell(self):
        self.execute('set +m')

    def detectShellEncoding(self):
        self.sendline('echo $LANG')
        lang = self._getLastOutput().strip()

        try:
            language, encoding = lang.split('.')
        except ValueError:
            raise ValueError, 'Invalid value for LANG: "%s"' % lang

        try:
            ''.decode(encoding)
        except LookupError:
            raise ValueError(
              'invalid encoding obtained by splitting language string "%s"' %
              lang
            )

        return encoding

    def _pre_prompt(self):
        pass

    def _pushTimeout(self, timeout):
        self.timeout_cache.append(self.timeout)
        self.timeout = timeout

    def _popTimeout(self):
        old_timeout = self.timeout
        try:
            new_timeout = self.timeout_cache.pop()
        except IndexError:
            pass
        else:
            self.timeout = new_timeout
        return old_timeout

    def _interpolate_command(self, cmd, *values, **kwargs):
        return self.interpolate(cmd.strip(), *values)

    def _adjust_command(self, interpolated_cmd, *values, **kwargs):
        force = kwargs.get('force', False)

        adjusted_cmd = interpolated_cmd

        if self.always_group_cmd:
            adjusted_cmd = '{ %s\n}' % adjusted_cmd

        if isinstance(adjusted_cmd, unicode):
            adjusted_cmd = self.encode(adjusted_cmd)

        if (not force) and (lengthOfLongestLine(adjusted_cmd) > 4095):
            raise ValueError(
              'Command contains a line longer than 4095 characters, '
              'and may not execute reliably. '
              'Use force = True if you really want this.'
            )

        return adjusted_cmd

    def _execute(self, cmd, *values, **kwargs):
        interpolated_cmd = self._interpolate_command(
          cmd, *values, **kwargs)
        adjusted_cmd = self._adjust_command(
          interpolated_cmd, *values, **kwargs)
        
        if self.trace:
            trace_cmd = interpolated_cmd
            if not isinstance(trace_cmd, unicode):
                trace_cmd = self.decode(trace_cmd)
            print trace_cmd

        bytes_sent = 0
        try:
            bytes_sent = self.sendline(adjusted_cmd)
        finally:
            if bytes_sent > len(adjusted_cmd):
                self._last_cmd = interpolated_cmd

    def _getLastOutput(self):
        self._pushTimeout(MAX_DELAY)
        try:
            self.expect(self.prompt_sep)
            return self.before.replace('\r\n', '\n').replace('\r', '\n')
        finally:
            self._popTimeout()

    def _getLastStatus(self):
        try:
            self._execute('echo $??')
            output = ''
            while not output.strip():
                output = self._getLastOutput()
            status = int(output)
            return status
        except Exception:
            self.interrupt()
            self.expect(self.prompt_sep)
            raise

    def _checkLastStatus(self, output):
        last_cmd = self._last_cmd
        status = self._getLastStatus()
        if self.failure_exceptions and (status > 0) and (status <= 128):
            raise CommandFailed(last_cmd, status, output)
        if self.signal_exceptions and (status > 128):
            raise CommandSignalled(last_cmd, status, output)
        return status

    def interrupt(self):
        # Send Control-C
        self.send('')

    def execute(self, cmd, *values, **kwargs):
        try:
            self._execute(cmd, *values, **kwargs)
            output = self._getLastOutput()
        except Exception:
            self.interrupt()
            self.expect(self.prompt_sep)
            raise
        output = self.decode(output)
        status = self._checkLastStatus(output)
        return status, output

    def encode(self, s):
        return s.encode(self.shell_encoding)

    def decode(self, s):
        return s.decode(self.shell_encoding)

    def _follow_chars(self, buffer):
        chars = [ ]
        while buffer and (not self.prompt.startswith(buffer)):
            chars.append(buffer[0])
            buffer = buffer[1:]

        if re.match(self.prompt_sep, buffer) is not None:
            raise CommandFinished

        return chars

    def _follow_bytes_iter(self, follow_input):
        if follow_input:
            for b in self.encode(self._last_cmd):
                yield b
            yield self.encode('\n')
        while True:
            yield self.read(1)

    def _follow(self, cmd, *values, **kwargs):
        buffer = ''

        follow_input = kwargs.get('follow_input', False)
        try:
            del kwargs['follow_input']
        except KeyError:
            pass

        try:
            self._execute(cmd, *values, **kwargs)

            self._pushTimeout(MAX_DELAY)
            try:
                for b in self._follow_bytes_iter(follow_input):
                    buffer = '%s%s' % (buffer, b)

                    try:
                        chars = self._follow_chars(buffer)
                    except CommandFinished:
                        break

                    for ch in chars:
                        if ch != '\r':
                            try:
                                yield ch
                            except Exception:
                                buffer = buffer[1:]
                                raise

                        buffer = buffer[1:]

            # BEGIN Python 2.3 compat
            #finally:
            except Exception:
                self._popTimeout()
                raise
            else:
                self._popTimeout()
            # END Python2.3 compat
        except Exception:
            self.interrupt()
            self.expect(self.prompt_sep)
            raise

    def _follow_decode(self, *args, **kwargs):
        buffer = ''

        for ch in self._follow(*args, **kwargs):
            buffer = '%s%s' % (buffer, ch)
            try:
                buffer_decoded = self.decode(buffer)
            except UnicodeDecodeError:
                continue
            else:
                for ch_decoded in buffer_decoded:
                    yield ch_decoded
                buffer = ''
                continue

    def follow(self, cmd, *values, **kwargs):
        for ch in self._follow_decode(cmd, *values, **kwargs):
            yield ch
        self._checkLastStatus(None)

    def followCallback(self, cmd, *values, **kwargs):
        try:
            callback = kwargs['callback']
            del kwargs['callback']
        except KeyError:
            raise TypeError, 'missing required argument "callback"'

        for ch in self._follow_decode(cmd, *values, **kwargs):
            callback(ch)

        return self._checkLastStatus(None)

    def followWrite(self, cmd, *values, **kwargs):
        outfile = kwargs.get('outfile', sys.stdout)
        try:
            del kwargs['outfile']
        except KeyError:
            pass

        def f(ch):
            outfile.write(ch)

        kwargs['callback'] = f
        return self.followCallback(cmd, *values, **kwargs)

    def followWriteReturn(self, cmd, *values, **kwargs):
        outfile = kwargs.get('outfile', sys.stdout)
        try:
            del kwargs['outfile']
        except KeyError:
            pass

        buffer = [ ]

        def f(ch):
            outfile.write(ch)
            buffer.append(ch)

        kwargs['callback'] = f

        try:
            status = self.followCallback(cmd, *values, **kwargs)
        except CommandFailed, e:
            output = ''.join(buffer)
            e.args = (e.args[0], e.args[1], output)
            raise e

        output = ''.join(buffer)
        return status, output

    def exit(self):
        self.sendeof()
        self.expect(EOF)
        self.close()

    def _getPrompt(self):
        return self._prompt
    def _setPrompt(self, value):
        self._prompt = value

        try:
            self._execute('PS1=?; export PS1', value)

            # FIXME: It'd be nice to do some sanity checking on the returned
            # output here:
            self._getLastOutput()

        except Exception:
            self.interrupt()

            # We can't expect anything reasonable in terms of prompt at this
            # point.  The shell is essentially unusable.  We simply raise the
            # exception and hope the caller can sort it out.
            #self.expect(self.prompt_sep)

            raise

    prompt = property(_getPrompt, _setPrompt)

    def _getPromptSep(self):
        return re.escape(self.prompt)
    prompt_sep = property(_getPromptSep)

    def _envValues_iter(self):
        try:
            self._execute('env')
            output = '%s%s%s' % (
              self._getLastOutput(),
              self.prompt,
              self._getLastOutput(),
            )
        except Exception:
            self.interrupt()
            self.expect(self.prompt_sep)
            raise
        output = self.decode(output)
        for line in output.strip().split('\n'):
            i = line.index('=')
            n, v = line[:i], line[i+1:]
            yield n, v

    def _envKeys(self):
        return list([n_v[0] for n_v in self._envValues_iter()])

    def _envGet(self, name):
        for n, v in self._envValues_iter():
            if n == name:
                return v
        raise KeyError

    def _envSet(self, name, value):
        self.execute('%s=?; export %s' % (name, name), value)

    def _envDel(self, name):
        self.execute('unset ?', name)

    def pwd(self):
        status, output = self.execute('echo $PWD')
        return output.strip()

    def pushd(self, dir):
        old_dir = self.pwd()
        self.dir_cache.append(self.pwd())
        try:
            self.cd(dir)
        except Exception:
            self.dir_cache.pop()
            raise
        return old_dir

    def popd(self):
        old_dir = self.pwd()
        new_dir = self.dir_cache.pop()
        try:
            self.cd(new_dir)
        except Exception:
            self.dir_cache.push(new_dir)
            raise
        return old_dir

    def cd(self, dir):
        return self.execute('cd ?', dir)

    def _interact_output_filter(self, s):
        if self.interactive_exit_string in s:
            raise ExitInteract
        return s

    def _sigwinch_handler(self, signum, frame):
        self.fit_window()

    def fit_window(self):
        s = struct.pack('HHHH', 0, 0, 0, 0)
        a = struct.unpack(
          'hhhh', fcntl.ioctl(sys.stdout.fileno(), termios.TIOCGWINSZ, s))
        self.setwinsize(a[0], a[1])

    def interact(self, fitted = False):
        if fitted:
            return self._interact_fitted()
        return self._interact()

    def interact_with(self, *args, **kwargs):
        fitted = kwargs.pop('fitted', False)
        if fitted:
            return self._interact_fitted(*args, **kwargs)
        return self._interact(*args, **kwargs)

    def _interact_fitted(self, *args, **kwargs):
        old_handler = signal.signal(signal.SIGWINCH, self._sigwinch_handler)
        try:
            self.fit_window()
            return self._interact(*args, **kwargs)
        finally:
            signal.signal(signal.SIGWINCH, old_handler)

    def _interact(self, cmd = None, *values, **kwargs):
        old_prompt = self.prompt

        self.execute('sh')
        self.setupShell()
        self.prompt = self.interactive_ps1
        self.execute('trap "echo \'%s\'" EXIT' % self.interactive_exit_string)

        # We're in a subshell, so we can do this without causing any parsing
        # issues or even having to clean up:
        self.sendline('PS2="%s"; export PS2' % self.interactive_ps2)
        self.expect(self.prompt_sep)
        self.sendline('PS3="%s"; export PS3' % self.interactive_ps3)
        self.expect(self.prompt_sep)
        self.sendline('PS4="%s"; export PS4' % self.interactive_ps4)
        self.expect(self.prompt_sep)

        if cmd is not None:
            cmd = u'%s; exit $??' % cmd
            interpolated_cmd = self._interpolate_command(
              cmd, *values, **kwargs)
            adjusted_cmd = self._adjust_command(
              interpolated_cmd, *values, **kwargs)
            self.sendline(adjusted_cmd)
            self._last_cmd = interpolated_cmd
        else:
            # Trigger first prompt to be displayed:
            self.sendline()

        self.setecho(True)
        try:
            try:
                super(Shell, self).interact(
                  escape_character = chr(0),
                  output_filter = self._interact_output_filter,
                )
            except ExitInteract:
                pass
        finally:
            print ''
            self.setecho(False)
        self._prompt = old_prompt
        try:
            self.expect(self.prompt_sep, timeout = 2)
        except TIMEOUT:
            self.sendline()
            self.expect(self.prompt_sep, timeout = 2)

        return self._checkLastStatus(None)

SORRY_TRY_AGAIN = 'Sorry, try again.'
PASSWORD_PROMPT = 'Password:'
ROOT_PROMPT = '#'

class SudoShell(Shell):
    password = None
    interactive_ps1 = '# '

    def __init__(self, shell = '/bin/sh', password = None, **kwargs):
        self.password = password
        # FIXME:  This will blow up if either PASSWORD_PROMPT or shell contains
        # double quote characters.
        super(SudoShell, self).__init__(
          shell = 'sudo -p "%s" "%s"' % (PASSWORD_PROMPT, shell), **kwargs)

    def _pre_prompt(self):
        root_prompt_regex = re.compile(re.escape(ROOT_PROMPT))
        password_prompt_regex = re.compile(re.escape(PASSWORD_PROMPT))
        sorry_try_again_regex = re.compile(re.escape(SORRY_TRY_AGAIN))

        self.expect([password_prompt_regex, root_prompt_regex])

        if password_prompt_regex.match(self.after):
            if self.password is None:
                raise ValueError, 'Password required'

            self.sendline(self.password)

            self.expect([root_prompt_regex, sorry_try_again_regex, EOF])
            if sorry_try_again_regex.match(self.after) or (self.after == EOF):
                raise ValueError, 'Password incorrect'

        if not root_prompt_regex.match(self.after):
            raise AssertionError, u'Expected string matching "%s", got "%s"' % (
              ROOT_PROMPT, self.after)

        del self.password
