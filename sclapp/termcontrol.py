# Copyright (c) 2006 Edward Loper.
# Copyright (c) 2007 Forest Bond.

# While this file is distributed with the sclapp software package, it is made
# available under the terms of a different license.  The source code contained
# in this module is a derivative of a recipe downloaded from the Active State
# Python Cookbook, the contents of which is published under the Python License.
# The original copyright has been maintained in this text; any additional
# copyrights are also included.

# The full text of the Python License can be obtained here:
# http://www.python.org/license

# The original recipe is available here:
# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/475116

# The relevant meta-information from that recipe follows:
# Title: Using terminfo for portable color output & cursor control
# Submitter: Edward Loper
# Last Updated: 2006/03/27
# Version no: 1.2
# Category: Text
# 
# Description:
# 
# The curses module defines several functions (based on terminfo) that can be
# used to perform lightweight cursor control & output formatting (color, bold,
# etc). These can be used without invoking curses mode (curses.initwin) or using
# any of the more heavy-weight curses functionality. This recipe defines a
# TerminalController class, which can make portable output formatting very
# simple. Formatting modes that are not supported by the terminal are simply
# omitted.

'''\
The curses module defines several functions (based on terminfo) that can be
used to perform lightweight cursor control & output formatting (color, bold,
etc). These can be used without invoking curses mode (curses.initwin) or using
any of the more heavy-weight curses functionality. This recipe defines a
TerminalController class, which can make portable output formatting very
simple. Formatting modes that are not supported by the terminal are simply
omitted.
'''

caps = { }

def empty(*args, **kwargs):
    return ''

def noop(*args, **kwargs):
    pass

def _tigetstr_curses(capname):
    # String capabilities can include "delays" of the form "$<2>".
    # For any modern terminal, we should be able to just ignore
    # these, so strip them out.
    import curses, re
    cap = curses.tigetstr(capname) or ''
    return re.sub(r'\$<\d+>[/*]?', '', cap)

def _tigetnum_curses(capname):
    import curses
    return curses.tigetnum(capname)

def _tigetnum_nocaps(capname):
    return None

def _setupterm_nocaps():
    return

def _setupterm_curses():
    try:
        import curses
    except ImportError:
        return
    return curses.setupterm()

def initializeTermControl():
    import sys

    _STRING_CAPABILITIES = {
      'BOL': 'cr',
      'UP': 'cuu1',
      'DOWN': 'cud1',
      'LEFT': 'cub1',
      'RIGHT': 'cuf1',
      'CLEAR_SCREEN': 'clear',
      'CLEAR_EOL': 'el',
      'CLEAR_BOL': 'el1',
      'CLEAR_EOS': 'ed',
      'BOLD': 'bold',
      'BLINK': 'blink',
      'DIM': 'dim',
      'REVERSE': 'rev',
      'UNDERLINE': 'smul',
      'NORMAL': 'sgr0',
      'HIDE_CURSOR': 'cinvis',
      'SHOW_CURSOR': 'cnorm',
    }
    _COLORS = [
      'BLACK', 'BLUE', 'GREEN', 'CYAN', 'RED', 'MAGENTA', 'YELLOW', 'WHITE']
    _ANSICOLORS = [
      'BLACK', 'RED', 'GREEN', 'YELLOW', 'BLUE', 'MAGENTA', 'CYAN', 'WHITE']

    __setupterm = _setupterm_curses
    __tigetstr = _tigetstr_curses
    __tigetnum = _tigetnum_curses

    # Curses isn't available on all platforms
    try:
        import curses
    except ImportError:
        __setupterm, __tigetstr, __tigetnum = noop, empty, noop

    # If the stream isn't a tty, then assume it has no capabilities.
    if not sys.stdout.isatty():
        __setupterm, __tigetstr, __tigetnum = noop, empty, noop

    __setupterm()

    # Look up numeric capabilities.
    caps['COLS'] = __tigetnum('cols')
    caps['LINES'] = __tigetnum('lines')
    
    # Look up string capabilities.
    for attrib, capname in _STRING_CAPABILITIES.items():
        caps[attrib] = __tigetstr(capname)

    # Colors
    set_fg = __tigetstr('setf')
    if set_fg:
        for i, color in zip(range(len(_COLORS)), _COLORS):
            caps[color] = curses.tparm(set_fg, i)
    set_fg_ansi = __tigetstr('setaf')
    if set_fg_ansi:
        for i, color in zip(range(len(_ANSICOLORS)), _ANSICOLORS):
            caps[color] = curses.tparm(set_fg_ansi, i)
    set_bg = __tigetstr('setb')
    if set_bg:
        for i, color in zip(range(len(_COLORS)), _COLORS):
            caps['BG_%s' % color] = curses.tparm(set_bg, i)
    set_bg_ansi = __tigetstr('setab')
    if set_bg_ansi:
        for i, color in zip(range(len(_ANSICOLORS)), _ANSICOLORS):
            caps['BG_%s' % color] = curses.tparm(set_bg_ansi, i)
