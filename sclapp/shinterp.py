# coding: utf-8

# Copyright (c) 2005-2007 Forest Bond.
# This file is part of the sclapp software package.
# 
# sclapp is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
# 
# A copy of the license has been included in the COPYING file.

import sys, itertools

def interpolate(s, *values, **kwargs):
    # FIXME: Should this be a unicode doc-string?
    r'''
    >>> interpolate('echo ?', 'foo')
    "echo 'foo'"

    >>> interpolate('echo ? ?', 'foo')
    Traceback (most recent call last):
    ...
    ValueError: Too few values to interpolate

    >>> interpolate('echo ? ?', 'foo', 'bar', 'baz')
    Traceback (most recent call last):
    ...
    ValueError: Too many values to interpolate

    >>> interpolate('echo ?', 'he\'llo')
    "echo 'he'\\''llo'"

    >>> interpolate('echo ? ?', 'hi', 'there')
    "echo 'hi' 'there'"

    >>> interpolate("echo 'foo'")
    "echo 'foo'"

    >>> interpolate(u'echo ?', 'foo')
    u"echo 'foo'"

    >>> interpolate('echo ?', u'foo')
    u"echo 'foo'"

    >>> interpolate('echo ?', u'•').encode('utf-8')
    "echo '\xc3\xa2\xc2\x80\xc2\xa2'"
    '''
    divisions = find_divisions(s)
    values = [ coerceToStringType(v).replace('\'', '\'\\\'\'') for v in values ]

    len_divisions, len_values = len(divisions), len(values)
    if len_divisions < (len_values + 1):
        raise ValueError, 'Too many values to interpolate'
    elif len_divisions > (len_values + 1):
        raise ValueError, 'Too few values to interpolate'

    total = zip_together(divisions, values)
    total.append(divisions[-1])
    return '\''.join(total)

def coerceToStringType(x):
    # If x is (like) a byte string, leave it as is.
    try:
        if str(x) == x:
            return x
    except UnicodeEncodeError:
        pass

    # If x is (like) a unicode string, leave it as is.
    try:
        if unicode(x) == x:
            return x
    except UnicodeDecodeError:
        pass

    # If we can make it a byte string, return that.
    try:
        return str(x)
    except UnicodeEncodeError:
        pass

    # If we can make it a unicode string, return that.
    try:
        return unicode(x)
    except UnicodeDecodeError:
        pass

    # Give up and return x as is, hoping that things work out.  Best of luck.
    return x

def izip_together(i1, i2):
    for t in itertools.izip(i1, i2):
        for x in t:
            yield x

def zip_together(l1, l2):
    return list(izip_together(l1, l2))

def find_divisions(s):
    '''
    >>> find_divisions('??one?two?three??')
    ['?one', 'two', 'three?']

    >>> find_divisions('??one??two??three??')
    ['?one?two?three?']

    >>> find_divisions('?one??two??three?')
    ['', 'one?two?three', '']
    '''
    # If we are given unicode, return unicode.
    # If we are given byte-strings, return byte-strings.
    s_type = type(s)
    parts = s.split(s_type('?'))

    new_parts = [ parts[0] ]

    iterable = iter(parts[1:])

    for part in iterable:
        if part != '':
            new_parts.append(part)
        else:
            first_half = new_parts.pop()

            try:
                new_parts.append(
                  s_type('%s?%s') % (first_half, iterable.next()))
            except StopIteration:
                new_parts.append(first_half)
                new_parts.append(s_type(''))

    return new_parts

if __name__ == '__main__':
    import doctest
    doctest.testmod()
